<?php
require_once ('inc/bases.php');
include_once ('inc/header.php');
?>
    <head>
        <title>404 not found</title>
    </head>
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/style_commun.css">

    <div class="error-page">
        <div>
            <h1 data-h1="403">403</h1>
            <p data-p="ACCESS DENIED">ACCESS DENIED</p>
        </div>
    </div>

    <div class="button-homepage">
        <a href="index.php" class="button-hp">Revenir sur la page d'accueil</a>
    </div>
<?php
include('inc/footer.php');